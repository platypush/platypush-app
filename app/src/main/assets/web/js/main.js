new Vue({
    el: '#app',
    data: function() {
        return {
            loading: false,
            splash: false,
            services: {
                scanned: [],
                saved: [],
            },
            addModal: {
                visible: false,
                host: undefined,
                port: 8008,
                name: undefined,
                save: false,
            },
        }
    },

    computed: {
        servicesByName: function() {
            return Object.values(this.services).reduce((obj, services) =>
                services.reduce((obj2, srv) => {
                    obj2[srv.name] = srv
                    return obj2
                }, obj), {})
        },

        servicesByHostAndPort: function() {
            return Object.values(this.services).reduce((obj, services) =>
                services.reduce((obj2, srv) => {
                    obj2[`${srv.host}:${srv.port}`] = srv
                    return obj2
                }, obj), {})
        },
    },

    methods: {
        refresh: function() {
            this.services.scanned = [
                ...this.services.scanned,
                ...JSON.parse(app.pollServices()).filter((srv) => !(srv.name in this.servicesByName)),
            ]
        },

        onServiceClick: function(service) {
            this.connect(service.host, service.port)
        },

        onServiceConnect: function() {
            if (!this.addModal.host?.length) {
                app.alert('Please specify a host name or IP address')
                return
            }

            if (this.addModal.save) {
                if (!this.saveService(this.addModal.host, this.addModal.port, this.addModal.name || ''))
                    return
            }

            this.connect(this.addModal.host, this.addModal.port)
        },

        saveService: function(host, port, name) {
            name = this.addModal.name.trim()
            if (!name.length) {
                app.alert('Please specify a name')
                return false
            }

            let overwrite = false
            if (name in this.servicesByName) {
                if (!confirm(`A service named ${name} already exists. ` +
                                 `Do you want to overwrite it?`))
                    return false

                overwrite = true
            } else if (`${host}:${port}` in this.servicesByHostAndPort) {
                if (!confirm(`A service on ${host}:${port} already exists. ` +
                                 `Do you want to overwrite it?`))
                    return false

                overwrite = true
            }

            this.loading = true
            try {
                const rs = app.saveService(host, port, name, overwrite)
                if (rs?.error)
                    throw rs.error

                this.loadServices()
                return true
            } finally {
                this.loading = false
            }
        },

        removeService: function(savedIndex, event) {
            event.stopPropagation()
            const srv = this.services.saved[savedIndex]
            if (!(srv && confirm('Are you sure that you want to remove this service?')))
                return false

            this.loading = true
            try {
                const rs = app.removeService(srv.host, srv.port, srv.name)
                if (rs?.error)
                    throw rs.error

                this.loadServices()
                return true
            } finally {
                this.loading = false
            }
        },

        connect: function(host, port) {
            this.loading = true
            app.stopServicesPoll()
            window.location.href = `http://${host}:${port}/`
        },

        splashScreen: function(duration) {
            const self = this
            this.splash = true
            setTimeout(() => {
                self.splash = false
            }, duration)
        },

        resetAddModal: function() {
            this.addModal = {
                host: undefined,
                port: 8008,
                name: undefined,
                save: false,
            }
        },

        loadServices: function() {
            this.services = {
                ...this.services,
                ...JSON.parse(app.loadServices())
            }
        },
    },

    mounted: function() {
        this.splashScreen(1500)

        this.$watch(() => this.addModal.visible, (newValue) => {
            if (!newValue)
                this.resetAddModal()
        })

        this.$watch(() => this.addModal.save, (newValue) => {
            if (newValue)
                this.addModal.name = this.addModal.host
        })

        this.loadServices()
        app.startServicesPoll()
        setInterval(this.refresh, 500)
    }
})

package tech.platypush.platypush

import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.webkit.JavascriptInterface
import org.json.JSONArray
import java.util.*


class WebAppInterface(context: Context) {
    private val serviceScanner = Scanner(context)
    private val serviceManager = Manager(context)

    @Suppress("unused")
    @JavascriptInterface
    fun startServicesPoll() {
        serviceScanner.startScan()
    }

    @Suppress("unused")
    @JavascriptInterface
    fun stopServicesPoll() {
        serviceScanner.stopScan()
    }

    @Suppress("unused")
    @JavascriptInterface
    fun pollServices(): String {
        val services = LinkedList<Map<String, Any?>>()
        for (srv in serviceScanner.getServices())
            services.add(srv.toMap())
        return JSONArray(services).toString()
    }

    @Suppress("unused")
    @JavascriptInterface
    fun loadServices(): String {
        return serviceManager.serializeServices(serviceManager.loadServices())
    }

    @Suppress("unused")
    @JavascriptInterface
    fun saveService(host: String, port: Int, name: String, overwrite: Boolean = false): String? {
        try {
            serviceManager.saveService(host, port, name, overwrite=overwrite)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            return JSON.dump(mapOf("error" to e.toString()))
        }

        return null
    }

    @Suppress("unused")
    @JavascriptInterface
    fun removeService(host: String, port: Int, name: String): String? {
        try {
            serviceManager.removeService(host, port, name)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            return JSON.dump(mapOf("error" to e.toString()))
        }

        return null
    }
}

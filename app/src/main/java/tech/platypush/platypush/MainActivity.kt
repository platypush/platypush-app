package tech.platypush.platypush

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebChromeClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webview.webViewClient = WebView()
        webview.webChromeClient = WebChromeClient()
        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true
        webview.clearCache(true)
        webview.addJavascriptInterface(WebAppInterface(this), "app")
        webview.loadUrl("file:///android_asset/web/index.html")
    }

    override fun onBackPressed() {
        if (webview != null && webview.canGoBack())
            webview.goBack()
        else
            super.onBackPressed()
    }
}

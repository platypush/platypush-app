package tech.platypush.platypush

import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


class JSON {
    companion object {
        private fun toMap(obj: JSONObject): Map<String, Any?> {
            val ret = HashMap<String, Any?>()
            val keys = obj.keys()

            while (keys.hasNext()) {
                val key = keys.next()
                var value = obj[key]

                if (value is JSONObject)
                    value = toMap(value)
                else if (value is JSONArray)
                    value = toList(value)

                ret[key] = value
            }

            return ret
        }

        private fun toList(arr: JSONArray): List<Any?> {
            val ret = LinkedList<Any?>()
            for (i in 0 until arr.length()) {
                var value = arr[i]

                if (value is JSONObject)
                    value = toMap(value)
                else if (value is JSONArray)
                    value = toList(value)

                ret.add(value)
            }

            return ret
        }

        fun load(str: String): Map<String, Any?> {
            return toMap(JSONObject(str))
        }

        fun dump(obj: Map<String, Any?>): String {
            return JSONObject(obj).toString()
        }
    }
}

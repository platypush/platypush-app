package tech.platypush.platypush

import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

class WebView : WebViewClient() {
    private fun shouldOverrideUrlLoadingInner(view: WebView, url: String): Boolean {
        view.loadUrl(url)
        return true
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        if (view == null || request == null)
            return super.shouldOverrideUrlLoading(view, request)

        return this.shouldOverrideUrlLoadingInner(view, request.url.toString())
    }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        @Suppress("DEPRECATION")
        if (view == null || url == null)
            return super.shouldOverrideUrlLoading(view, url)

        return this.shouldOverrideUrlLoadingInner(view, url)
    }
}

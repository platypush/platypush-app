# Changelog
All notable changes to this project will be documented in this file.

## [1.0.1] - 2021-04-28
### Added
- Support for saved/favourite hosts and services.

### Changed
- App migrated from AndroidJS to native Kotlin+webview (and APK size dropped to ~4MB).

### Fixed
- Improved speed and stability of services scan.

## [1.0.0] - 2021-02-26
### Added
- Automatic discovery of Platypush web services on the network.
- Manual connection to a service not found on the network.
- Transparent interface to the responsive web view provided by the Platypush web server.

This repository contains the source for the Platypush mobile app. As of now only Android is supported.

## Build

Debug mode:

```shell
./gradlew assembleDebug
# APK generated under app/build/outputs/apk/debug/app-debug.apk
```

Release mode:

```shell
./gradlew assembleRelease
# APK generated under app/build/outputs/apk/release/app-release-unsigned.apk
```

